import React, { Component } from "react";
import { connect } from "react-redux";

class DetailComponent extends Component {
  render() {
    // console.log(this.props);
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.detail;
    return (
      <div className="row">
        <div className="col-4">
          <img src={hinhAnh} alt="" style={{ width: "100%" }} />
        </div>
        <div className="col-8">
          <h4>ID: {maSP}</h4>
          <h4>Name: {tenSP}</h4>
          <h4>Screen: {manHinh}</h4>
          <h4>OS: {heDieuHanh}</h4>
          <h4>Back camera: {cameraSau}</h4>
          <h4>Front camera{cameraTruoc}</h4>
          <h4>RAM: {ram}</h4>
          <h4>ROM: {rom}</h4>
          <h4>Price: {giaBan}$</h4>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.phone.detail,
  };
};

export default connect(mapStateToProps)(DetailComponent);

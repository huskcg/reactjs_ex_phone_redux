import { VIEW_DETAIL } from "../constant/phoneConstant";
import { data } from "../data/data";

let initialValue = {
  data: data,
  detail: data[2],
};

export const phoneReducer = (state = initialValue, action) => {
  switch (action.type) {
    case VIEW_DETAIL: {
      const detail = state.data.find((item) => item.maSP === action.payload);
      return { ...state, detail: detail };
    }
    default:
      return state;
  }
};
